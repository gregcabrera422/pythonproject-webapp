from django.shortcuts import render
from .models import Posts
# Create your views here.
def index(request):
    posts = Posts.objects.all()

    context = {
        'title': 'Your Posts',
        'posts': posts
    }
    return render(request, 'posts/index.html', context)

def details(request, id):
    posts = Posts.objects.get(id=id)
    context = {
        'post': posts
    }

    return render(request, 'posts/details.html', context)
